<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    #[Route('/about', name: 'about')]
    public function index(): Response
    {
        return $this->render('about/index.html.twig', [
            'controller_name' => 'AboutController',
            'title' => 'Об авторе',
            'git' => 'https://gitlab.com/clumsyfl/',
            'name' => 'Усков Даниил',
            'info' => 'Образование : ТюмГУ, ИМиКН, ИСиТ, 2017-2021'
        ]);
    }
}
