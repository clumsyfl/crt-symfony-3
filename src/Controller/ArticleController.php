<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route('/article/{id}', name: 'article_detail')]
    public function index(int $id, Request $request, Article $article, ArticleRepository $articleRepository): Response
    {
        $articleqq = $articleRepository->find($id);
        if ($articleqq === null) {
            throw new \RuntimeException("Article with ID: {$id}. Does not exists");
        }

        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
            'article' => $articleqq,
            'id'=>$id
        ]);
    }
}
