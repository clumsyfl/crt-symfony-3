--
-- PostgreSQL database dump
--

-- Dumped from database version 11.14
-- Dumped by pg_dump version 11.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: article; Type: TABLE; Schema: public; Owner: dbuser
--

CREATE TABLE public.article (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    shortret character varying(255) NOT NULL,
    text text NOT NULL,
    image character varying(255) NOT NULL,
    date date NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.article OWNER TO dbuser;

--
-- Name: article_id_seq; Type: SEQUENCE; Schema: public; Owner: dbuser
--

CREATE SEQUENCE public.article_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_id_seq OWNER TO dbuser;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: dbuser
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO dbuser;

--
-- Name: user; Type: TABLE; Schema: public; Owner: dbuser
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE public."user" OWNER TO dbuser;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: dbuser
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO dbuser;

--
-- Data for Name: article; Type: TABLE DATA; Schema: public; Owner: dbuser
--

COPY public.article (id, name, shortret, text, image, date, updated_at) FROM stdin;
6	art name	art shortret	art text art text art text art text art text art text art text	1.png	2021-12-24	2021-12-03 17:32:00
7	Статья 7	Краткое содержание статьи 7	Текст статьи 7 Текст статьи 7 Текст статьи 7 Текст статьи 7 Текст статьи 7 Текст статьи 7 Текст статьи 7	bs.png	2021-12-15	2021-12-15 00:23:00
8	8	8888888	<div>88888 8888888 88888</div>	forUP.png	2021-12-08	2021-12-15 17:02:41
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: dbuser
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20211209125231	2021-12-09 12:52:36	52
DoctrineMigrations\\Version20211210115446	2021-12-10 11:55:02	26
DoctrineMigrations\\Version20211210124255	2021-12-10 12:43:29	32
DoctrineMigrations\\Version20211213123411	2021-12-13 12:37:14	102
DoctrineMigrations\\Version20211214122513	2021-12-14 12:25:36	117
DoctrineMigrations\\Version20211214123002	2021-12-14 12:30:08	105
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: dbuser
--

COPY public."user" (id, email, roles, password) FROM stdin;
1	q@mail.ru	[]	123
2	1@mail.ru	[]	$2y$13$lLps321CFAtybloS25NRp.IfpqV6bKO/3..cw6nSA7rspH6BChEPC
3	11@mail.ru	[]	$2y$13$vUIj9H55OTkLoE7bNZlDyu/P/2MOcPQZuAmD.e5L6XLzyuWDMi4.2
4	qwe@mail.ru	[]	$2y$13$Oc6lv5CNPcVS3nlj5Eb7SuIIm1sn2vqIFCSacFxPNblVe6cNK/WFK
\.


--
-- Name: article_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dbuser
--

SELECT pg_catalog.setval('public.article_id_seq', 8, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dbuser
--

SELECT pg_catalog.setval('public.user_id_seq', 4, true);


--
-- Name: article article_pkey; Type: CONSTRAINT; Schema: public; Owner: dbuser
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: dbuser
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: dbuser
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: uniq_8d93d649e7927c74; Type: INDEX; Schema: public; Owner: dbuser
--

CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON public."user" USING btree (email);


--
-- PostgreSQL database dump complete
--

